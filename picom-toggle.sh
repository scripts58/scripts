#!/bin/bash

# Description: This script toggles the X compositor picom. If it is running it will kill the process otherwise it will start it.

# Dependencies: picom and notify if you wont to use notifications

declare is_notify_enabled=false

function echo_red () {
  echo "$1" | while read -r line; do echo -e "\e[01;31m$line\e[0m"; done
}

function error () {
  echo_red "$1" >&2
}

function turnon () {
  # picom -b --config ~/.config/picom/picom.conf
  picom --experimental-backends -b
  if $is_notify_enabled; then
    notify-send "picom enabled"
  fi
}

function turnoff () {
  killall picom
  if $is_notify_enabled; then
    notify-send "picom disabled"
  fi
}

function toggle() {
  if pgrep -x "picom" > /dev/null; then
    turnoff
  else
    turnon
  fi
}

function handle_flags() {

  # flags
  declare turnon=false
  declare turnoff=false
  declare toggle=false
  declare no_notification=false
  declare notification=false

  while getopts "oftnN" OPTION; do
    case "$OPTION" in
      o) turnon=true          ;;
      f) turnoff=true         ;;
      t) toggle=true          ;;
      N) no_notification=true ;;
      n) notification=true    ;;
      *)
        echo "script usage: $(basename \$0) [-o] [-f] [-n] [-N]" >&2
        exit
      ;;
    esac
  done 2> >(error "$(cat)")

  # set whether notifications are enabled
  # NOTE: -n overrides -N if both are set
  if $no_notification && ! $notification; then
    is_notify_enabled=false
  elif $notification; then
    is_notify_enabled=true
  fi

  # count set action options (such as turn on or toggle)
  ((count_action_options=0))
  for e in $turnon $turnoff $toggle; do
    if $e; then
      ((count_action_options+=1))
    fi
  done

  # if no action option is specified go with the default (toggle)
  if ((count_action_options == 0)); then
    toggle
    exit
  fi

  # if more then one action is specified throw error
  if ((count_action_options > 1)); then
    error "you need to specify only one action option such as toggle [-t]\n default action is toggle"
    exit
  fi


  # perform the chosen action and exit
  if $toggle; then
    toggle
    exit
  fi

  if $turnon; then
    turnon
    exit
  fi

  if $turnoff; then
    turnoff
    exit
  fi
}

handle_flags "$@"
