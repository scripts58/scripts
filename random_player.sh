#!/bin/bash

# Description: random play song from an directory with songs or
# from an file with one path to an song in each line.

# Dependencies: mpv

declare player_cmd="mpv" # you can set another media player here

function echo_red () {
  echo "$1" | while read -r line; do echo -e "\e[01;31m$line\e[0m"; done
}

function error () {
  echo_red "$1" >&2
}

function play_random_song() {
  declare extract_songs_cmd
  if [ -d "$1" ]; then
    extract_songs_cmd="find $1 -type f"
  elif [ -f "$1" ]; then
    extract_songs_cmd="cat $1"
  else
    error "$1 is no file or directory"
    exit
  fi

  declare -a songs=()
  while IFS= read -r line; do
    songs+=( "$line" )
  done < <( $extract_songs_cmd )

  declare index=$((RANDOM % (${#songs[@]} + 1)))
  declare song=${songs[$index]}

  clear
  echo "#########################################################"
  echo "play song: $(basename "$song")"
  echo "#########################################################"
  echo
  $player_cmd "$song"
}

while true; do
  play_random_song "$@" || {
    error "failed to play song\n"
  }
done
